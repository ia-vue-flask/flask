from flask import Flask, jsonify, request
from flask_cors import CORS
import pickle as pk
import numpy as np
import json
from json import JSONEncoder


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)


app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/tree_classifier')
def predictDecisionTreeClassifier():
    if request.method == 'GET':
        sepalLength = request.args.get('sepalLength')
        sepalWidth = request.args.get('sepalWidth')
        pepalLength = request.args.get('petalLength')
        pepalWidth = request.args.get('petalWidth')
        X = np.array([[sepalLength, sepalWidth, pepalLength, pepalWidth]])
        # Abrir el modelo
        pickle_file = open(
            'src/model/modelo_DecisionTreeClassifier_iris', 'rb')
        modelo = pk.load(pickle_file)
        response = modelo.predict(X)
        numpyData = {"response": response}
        # use dump() to write array into file
        encodedNumpyData = json.dumps(numpyData, cls=NumpyArrayEncoder)
        return encodedNumpyData
    return {}


@app.route('/svc')
def predictSvc():
    if request.method == 'GET':
        sepalLength = request.args.get('sepalLength')
        sepalWidth = request.args.get('sepalWidth')
        pepalLength = request.args.get('petalLength')
        pepalWidth = request.args.get('petalWidth')
        X = np.array([[sepalLength, sepalWidth, pepalLength, pepalWidth]])
        # Abrir el modelo
        pickle_file = open('src/model/modelo_SVC_iris', 'rb')
        modelo = pk.load(pickle_file)
        response = modelo.predict(X)
        numpyData = {"response": response}
        # use dump() to write array into file
        encodedNumpyData = json.dumps(numpyData, cls=NumpyArrayEncoder)
        return encodedNumpyData
    return {}

@app.route('/healthcheck')
def healthcheck():
    if request.method == 'GET':
        return "I'm lived" 


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=4000, debug=True)
