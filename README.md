# Python version 3.8

----------------------------------

pip freeze > requirements.txt  -> extraer las dependencias de py a un archivo requirements.txt

----------------------------------
docker build -t python .   -> crear container con nombre flaskapp y con los archivos de raiz
docker run -it -p 7000:4000 --name flask_container -d flaskapp -> ejecutar la app en docker

docker build -t python . && docker run -it -p 7000:4000 --name flask_container -d python && docker start flask_container


---------------------------------
docker run -it flaskapp /bin/sh -> ejecutar el container flaskapp en modo interactivo